From: Werner Koch <wk@gnupg.org>
Date: Wed, 8 Mar 2017 10:46:09 +0100
Subject: doc: Add a note to the trust model direct.

* doc/gpg.texi (GPG Configuration Options): Add note.  Chnage Index
from trust-mode:foo to trust-model:foo.

(cherry picked from commit f0257b4a86b73f5b956028e68590b6d2a23ea4da)
---
 doc/gpg.texi | 20 ++++++++++++--------
 1 file changed, 12 insertions(+), 8 deletions(-)

diff --git a/doc/gpg.texi b/doc/gpg.texi
index 3b82b4457..d65873756 100644
--- a/doc/gpg.texi
+++ b/doc/gpg.texi
@@ -1600,17 +1600,17 @@ Set what trust model GnuPG should follow. The models are:
 @table @asis
 
   @item pgp
-  @opindex trust-mode:pgp
+  @opindex trust-model:pgp
   This is the Web of Trust combined with trust signatures as used in PGP
   5.x and later. This is the default trust model when creating a new
   trust database.
 
   @item classic
-  @opindex trust-mode:classic
+  @opindex trust-model:classic
   This is the standard Web of Trust as introduced by PGP 2.
 
   @item tofu
-  @opindex trust-mode:tofu
+  @opindex trust-model:tofu
   @anchor{trust-model-tofu}
   TOFU stands for Trust On First Use.  In this trust model, the first
   time a key is seen, it is memorized.  If later another key is seen
@@ -1656,7 +1656,7 @@ Set what trust model GnuPG should follow. The models are:
   @code{undefined} trust level is returned.
 
   @item tofu+pgp
-  @opindex trust-mode:tofu+pgp
+  @opindex trust-model:tofu+pgp
   This trust model combines TOFU with the Web of Trust.  This is done
   by computing the trust level for each model and then taking the
   maximum trust level where the trust levels are ordered as follows:
@@ -1669,12 +1669,16 @@ Set what trust model GnuPG should follow. The models are:
   which some security-conscious users don't like.
 
   @item direct
-  @opindex trust-mode:direct
+  @opindex trust-model:direct
   Key validity is set directly by the user and not calculated via the
-  Web of Trust.
+  Web of Trust.  This model is soley based on the key and does
+  not distinguish user IDs.  Note that when changing to another trust
+  model the trust values assigned to a key are transformed into
+  ownertrust values, which also indicate how you trust the owner of
+  the key to sign other keys.
 
   @item always
-  @opindex trust-mode:always
+  @opindex trust-model:always
   Skip key validation and assume that used keys are always fully
   valid. You generally won't use this unless you are using some
   external validation scheme. This option also suppresses the
@@ -1684,7 +1688,7 @@ Set what trust model GnuPG should follow. The models are:
   disabled keys.
 
   @item auto
-  @opindex trust-mode:auto
+  @opindex trust-model:auto
   Select the trust model depending on whatever the internal trust
   database says. This is the default model if such a database already
   exists.
