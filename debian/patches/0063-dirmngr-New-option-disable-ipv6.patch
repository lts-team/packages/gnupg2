From: Werner Koch <wk@gnupg.org>
Date: Mon, 3 Apr 2017 20:56:12 +0200
Subject: dirmngr: New option --disable-ipv6

* dirmngr/dirmngr.h (struct opt): Add field 'disable_ipv6'.
* dirmngr/dirmngr.c (oDisableIPv6): New const.
(opts): New option --disable-ipv6.
(parse_rereadable_options): Set that option.
* dirmngr/dns-stuff.c (opt_disable_ipv6): New var.
(set_dns_disable_ipv6): New.
(resolve_name_standard): Make use of it.
* dirmngr/ks-engine-finger.c (ks_finger_fetch): Take care of
OPT.DISABLE_IPV6.
* dirmngr/ks-engine-hkp.c (map_host): Ditto.
(send_request): Ditto.
* dirmngr/ks-engine-http.c (ks_http_fetch): Ditto.
* dirmngr/ocsp.c (do_ocsp_request): Ditto.

Signed-off-by: Werner Koch <wk@gnupg.org>
(cherry picked from commit 3533b854408fa93734742b2ee12b62aa0d55ff28)
---
 dirmngr/crlfetch.c         |  1 +
 dirmngr/dirmngr.c          |  4 ++++
 dirmngr/dirmngr.h          |  3 ++-
 dirmngr/dns-stuff.c        | 15 +++++++++++++++
 dirmngr/dns-stuff.h        |  4 ++++
 dirmngr/ks-engine-finger.c |  3 ++-
 dirmngr/ks-engine-hkp.c    |  7 +++++--
 dirmngr/ks-engine-http.c   |  3 ++-
 dirmngr/ocsp.c             |  3 ++-
 doc/dirmngr.texi           |  5 +++--
 10 files changed, 40 insertions(+), 8 deletions(-)

diff --git a/dirmngr/crlfetch.c b/dirmngr/crlfetch.c
index 337fe6e4d..2700cf932 100644
--- a/dirmngr/crlfetch.c
+++ b/dirmngr/crlfetch.c
@@ -200,6 +200,7 @@ crl_fetch (ctrl_t ctrl, const char *url, ksba_reader_t *reader)
                                    |(DBG_LOOKUP? HTTP_FLAG_LOG_RESP:0)
                                    |(dirmngr_use_tor()? HTTP_FLAG_FORCE_TOR:0)
                                    |(opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4:0)
+                                   |(opt.disable_ipv6? HTTP_FLAG_IGNORE_IPv6:0)
                                    ),
                                   ctrl->http_proxy, NULL, NULL, NULL);
 
diff --git a/dirmngr/dirmngr.c b/dirmngr/dirmngr.c
index 43e9cbd07..31d3ca235 100644
--- a/dirmngr/dirmngr.c
+++ b/dirmngr/dirmngr.c
@@ -112,6 +112,7 @@ enum cmd_and_opt_values {
   oDisableHTTP,
   oDisableLDAP,
   oDisableIPv4,
+  oDisableIPv6,
   oIgnoreLDAPDP,
   oIgnoreHTTPDP,
   oIgnoreOCSPSvcUrl,
@@ -228,6 +229,7 @@ static ARGPARSE_OPTS opts[] = {
   ARGPARSE_s_n (oNoUseTor, "no-use-tor", "@"),
 
   ARGPARSE_s_n (oDisableIPv4, "disable-ipv4", "@"),
+  ARGPARSE_s_n (oDisableIPv6, "disable-ipv6", "@"),
 
   ARGPARSE_s_s (oSocketName, "socket-name", "@"),  /* Only for debugging.  */
 
@@ -624,6 +626,7 @@ parse_rereadable_options (ARGPARSE_ARGS *pargs, int reread)
     case oDisableHTTP: opt.disable_http = 1; break;
     case oDisableLDAP: opt.disable_ldap = 1; break;
     case oDisableIPv4: opt.disable_ipv4 = 1; break;
+    case oDisableIPv6: opt.disable_ipv6 = 1; break;
     case oHonorHTTPProxy: opt.honor_http_proxy = 1; break;
     case oHTTPProxy: opt.http_proxy = pargs->r.ret_str; break;
     case oLDAPProxy: opt.ldap_proxy = pargs->r.ret_str; break;
@@ -690,6 +693,7 @@ parse_rereadable_options (ARGPARSE_ARGS *pargs, int reread)
   set_dns_verbose (opt.verbose, !!DBG_DNS);
   http_set_verbose (opt.verbose, !!DBG_NETWORK);
   set_dns_disable_ipv4 (opt.disable_ipv4);
+  set_dns_disable_ipv6 (opt.disable_ipv6);
 
   return 1; /* Handled. */
 }
diff --git a/dirmngr/dirmngr.h b/dirmngr/dirmngr.h
index 6a4fd003f..4cc2be0a9 100644
--- a/dirmngr/dirmngr.h
+++ b/dirmngr/dirmngr.h
@@ -97,7 +97,8 @@ struct
 
   int disable_http;       /* Do not use HTTP at all.  */
   int disable_ldap;       /* Do not use LDAP at all.  */
-  int disable_ipv4;       /* Do not use leagacy IP addresses.  */
+  int disable_ipv4;       /* Do not use legacy IP addresses.  */
+  int disable_ipv6;       /* Do not use standard IP addresses.  */
   int honor_http_proxy;   /* Honor the http_proxy env variable. */
   const char *http_proxy; /* The default HTTP proxy.  */
   const char *ldap_proxy; /* Use given LDAP proxy.  */
diff --git a/dirmngr/dns-stuff.c b/dirmngr/dns-stuff.c
index 150237e53..ed77742b4 100644
--- a/dirmngr/dns-stuff.c
+++ b/dirmngr/dns-stuff.c
@@ -123,6 +123,10 @@ static int opt_timeout;
  * returned A records.  */
 static int opt_disable_ipv4;
 
+/* The flag to disable IPv6 access - right now this only skips
+ * returned AAAA records.  */
+static int opt_disable_ipv6;
+
 /* If set force the use of the standard resolver.  */
 static int standard_resolver;
 
@@ -248,6 +252,15 @@ set_dns_disable_ipv4 (int yes)
 }
 
 
+/* Set the Disable-IPv6 flag so that the name resolver does not return
+ * AAAA addresses.  */
+void
+set_dns_disable_ipv6 (int yes)
+{
+  opt_disable_ipv6 = !!yes;
+}
+
+
 /* Set the timeout for libdns requests to SECONDS.  A value of 0 sets
  * the default timeout and values are capped at 10 minutes.  */
 void
@@ -934,6 +947,8 @@ resolve_name_standard (const char *name, unsigned short port,
         continue;
       if (opt_disable_ipv4 && ai->ai_family == AF_INET)
         continue;
+      if (opt_disable_ipv6 && ai->ai_family == AF_INET6)
+        continue;
 
       dai = xtrymalloc (sizeof *dai + ai->ai_addrlen - 1);
       dai->family = ai->ai_family;
diff --git a/dirmngr/dns-stuff.h b/dirmngr/dns-stuff.h
index 9b8303c3b..71605b741 100644
--- a/dirmngr/dns-stuff.h
+++ b/dirmngr/dns-stuff.h
@@ -99,6 +99,10 @@ void set_dns_verbose (int verbose, int debug);
  * A addresses.  */
 void set_dns_disable_ipv4 (int yes);
 
+/* Set the Disable-IPv6 flag so that the name resolver does not return
+ * AAAA addresses.  */
+void set_dns_disable_ipv6 (int yes);
+
 /* Set the timeout for libdns requests to SECONDS.  */
 void set_dns_timeout (int seconds);
 
diff --git a/dirmngr/ks-engine-finger.c b/dirmngr/ks-engine-finger.c
index 811b72de4..8a21c9f40 100644
--- a/dirmngr/ks-engine-finger.c
+++ b/dirmngr/ks-engine-finger.c
@@ -84,7 +84,8 @@ ks_finger_fetch (ctrl_t ctrl, parsed_uri_t uri, estream_t *r_fp)
 
   err = http_raw_connect (&http, server, 79,
                           ((dirmngr_use_tor ()? HTTP_FLAG_FORCE_TOR : 0)
-                           | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
+                           | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)
+                           | (opt.disable_ipv6? HTTP_FLAG_IGNORE_IPv6 : 0)),
                           NULL);
   if (err)
     {
diff --git a/dirmngr/ks-engine-hkp.c b/dirmngr/ks-engine-hkp.c
index 66350a7bc..7c91b6a36 100644
--- a/dirmngr/ks-engine-hkp.c
+++ b/dirmngr/ks-engine-hkp.c
@@ -568,6 +568,8 @@ map_host (ctrl_t ctrl, const char *name, const char *srvtag, int force_reselect,
                 continue;
               if (opt.disable_ipv4 && ai->family == AF_INET)
                 continue;
+              if (opt.disable_ipv6 && ai->family == AF_INET6)
+                continue;
               dirmngr_tick (ctrl);
 
               add_host (name, is_pool, ai, 0, reftbl, reftblsize, &refidx);
@@ -649,7 +651,7 @@ map_host (ctrl_t ctrl, const char *name, const char *srvtag, int force_reselect,
         {
           for (ai = aibuf; ai; ai = ai->next)
             {
-              if (ai->family == AF_INET6
+              if ((!opt.disable_ipv6 && ai->family == AF_INET6)
                   || (!opt.disable_ipv4 && ai->family == AF_INET))
                 {
                   err = resolve_dns_addr (ai->addr, ai->addrlen, 0, &host);
@@ -1102,7 +1104,8 @@ send_request (ctrl_t ctrl, const char *request, const char *hostportstr,
                    (httpflags
                     |(opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
                     |(dirmngr_use_tor ()? HTTP_FLAG_FORCE_TOR:0)
-                    |(opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
+                    |(opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)
+                    |(opt.disable_ipv6? HTTP_FLAG_IGNORE_IPv6 : 0)),
                    ctrl->http_proxy,
                    session,
                    NULL,
diff --git a/dirmngr/ks-engine-http.c b/dirmngr/ks-engine-http.c
index 69642ff98..6de061699 100644
--- a/dirmngr/ks-engine-http.c
+++ b/dirmngr/ks-engine-http.c
@@ -89,7 +89,8 @@ ks_http_fetch (ctrl_t ctrl, const char *url, estream_t *r_fp)
                    /* fixme: AUTH */ NULL,
                    ((opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
                     | (dirmngr_use_tor ()? HTTP_FLAG_FORCE_TOR:0)
-                    | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
+                    | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)
+                    | (opt.disable_ipv6? HTTP_FLAG_IGNORE_IPv6 : 0)),
                    ctrl->http_proxy,
                    session,
                    NULL,
diff --git a/dirmngr/ocsp.c b/dirmngr/ocsp.c
index aff8e3288..22391c32d 100644
--- a/dirmngr/ocsp.c
+++ b/dirmngr/ocsp.c
@@ -175,7 +175,8 @@ do_ocsp_request (ctrl_t ctrl, ksba_ocsp_t ocsp, gcry_md_hd_t md,
   err = http_open (&http, HTTP_REQ_POST, url, NULL, NULL,
                    ((opt.honor_http_proxy? HTTP_FLAG_TRY_PROXY:0)
                     | (dirmngr_use_tor ()? HTTP_FLAG_FORCE_TOR:0)
-                    | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)),
+                    | (opt.disable_ipv4? HTTP_FLAG_IGNORE_IPv4 : 0)
+                    | (opt.disable_ipv6? HTTP_FLAG_IGNORE_IPv6 : 0)),
                    ctrl->http_proxy, NULL, NULL, NULL);
   if (err)
     {
diff --git a/doc/dirmngr.texi b/doc/dirmngr.texi
index b00c2d377..9a7238fb5 100644
--- a/doc/dirmngr.texi
+++ b/doc/dirmngr.texi
@@ -313,9 +313,10 @@ a numerical IP address must be given (IPv6 or IPv4) and that no error
 checking is done for @var{ipaddr}.
 
 @item --disable-ipv4
+@item --disable-ipv6
 @opindex disable-ipv4
-Disable the use of all IPv4 addresses.  This option is mainly useful
-for debugging.
+@opindex disable-ipv6
+Disable the use of all IPv4 or IPv6 addresses.
 
 @item --disable-ldap
 @opindex disable-ldap
