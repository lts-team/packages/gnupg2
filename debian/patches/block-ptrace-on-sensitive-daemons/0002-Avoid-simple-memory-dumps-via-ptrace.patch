From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Tue, 11 Aug 2015 20:28:26 -0400
Subject: Avoid simple memory dumps via ptrace

This avoids needing to setgid gpg-agent or scdaemon.  It probably
doesn't defend against all possible attacks, but it defends against
one specific (and easy) one.  If there are other protections we should
do them too.

This will make it slightly harder to debug the agent or scdaemon
because the normal user won't be able to attach gdb to it directly
while it runs.

The remaining options for debugging are:

 * launch gpg-agent or scdaemon from gdb directly
 * connect gdb to a running gpg-agent or scdaemon as the superuser

Upstream bug: https://bugs.gnupg.org/gnupg/issue1211
---
 agent/gpg-agent.c | 8 ++++++++
 configure.ac      | 1 +
 scd/scdaemon.c    | 9 +++++++++
 3 files changed, 18 insertions(+)

diff --git a/agent/gpg-agent.c b/agent/gpg-agent.c
index c0208cc88..31bf3370a 100644
--- a/agent/gpg-agent.c
+++ b/agent/gpg-agent.c
@@ -48,6 +48,9 @@
 # include <signal.h>
 #endif
 #include <npth.h>
+#ifdef HAVE_PRCTL
+# include <sys/prctl.h>
+#endif
 
 #define GNUPG_COMMON_NEED_AFLOCAL
 #include "agent.h"
@@ -949,6 +952,11 @@ main (int argc, char **argv )
 
   early_system_init ();
 
+#if defined(HAVE_PRCTL) && defined(PR_SET_DUMPABLE)
+  /* Disable ptrace on Linux without sgid bit */
+  prctl(PR_SET_DUMPABLE, 0);
+#endif
+
   /* Before we do anything else we save the list of currently open
      file descriptors and the signal mask.  This info is required to
      do the exec call properly.  We don't need it on Windows.  */
diff --git a/configure.ac b/configure.ac
index f929cb60f..f2b6a70d2 100644
--- a/configure.ac
+++ b/configure.ac
@@ -1335,6 +1335,7 @@ AC_CHECK_FUNCS([strerror strlwr tcgetattr mmap canonicalize_file_name])
 AC_CHECK_FUNCS([strcasecmp strncasecmp ctermid times gmtime_r strtoull])
 AC_CHECK_FUNCS([setenv unsetenv fcntl ftruncate inet_ntop])
 AC_CHECK_FUNCS([canonicalize_file_name])
+AC_CHECK_FUNCS([prctl])
 AC_CHECK_FUNCS([gettimeofday getrusage getrlimit setrlimit clock_gettime])
 AC_CHECK_FUNCS([atexit raise getpagesize strftime nl_langinfo setlocale])
 AC_CHECK_FUNCS([waitpid wait4 sigaction sigprocmask pipe getaddrinfo])
diff --git a/scd/scdaemon.c b/scd/scdaemon.c
index 74fed4454..4d011c4c9 100644
--- a/scd/scdaemon.c
+++ b/scd/scdaemon.c
@@ -36,6 +36,9 @@
 #include <unistd.h>
 #include <signal.h>
 #include <npth.h>
+#ifdef HAVE_PRCTL
+# include <sys/prctl.h>
+#endif
 
 #define GNUPG_COMMON_NEED_AFLOCAL
 #include "scdaemon.h"
@@ -409,6 +412,12 @@ main (int argc, char **argv )
   npth_t pipecon_handler;
 
   early_system_init ();
+
+#if defined(HAVE_PRCTL) && defined(PR_SET_DUMPABLE)
+  /* Disable ptrace on Linux without sgid bit */
+  prctl(PR_SET_DUMPABLE, 0);
+#endif
+
   set_strusage (my_strusage);
   gcry_control (GCRYCTL_SUSPEND_SECMEM_WARN);
   /* Please note that we may running SUID(ROOT), so be very CAREFUL
