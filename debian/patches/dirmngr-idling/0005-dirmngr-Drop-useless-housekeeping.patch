From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Sat, 29 Oct 2016 02:15:08 -0400
Subject: dirmngr: Drop useless housekeeping.

* dirmngr/dirmngr.c (handle_tick, time_for_housekeeping_p,
housekeeping_thread): Remove, no longer needed.
(handle_connections): Drop any attempt at a timeout, since no
housekeeping is necessary.

--

The housekeeping thread no longer does anything, and the main loop was
waking up every 60 seconds for no good reason.  The code is simpler
and the runtime is more efficient if we drop this.

Signed-off-by: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
---
 dirmngr/dirmngr.c | 113 +++---------------------------------------------------
 1 file changed, 5 insertions(+), 108 deletions(-)

diff --git a/dirmngr/dirmngr.c b/dirmngr/dirmngr.c
index 746f43d10..8d9de9e5a 100644
--- a/dirmngr/dirmngr.c
+++ b/dirmngr/dirmngr.c
@@ -304,13 +304,6 @@ static int active_connections;
  * thread to run background network tasks.  */
 static int network_activity_seen;
 
-/* The timer tick used for housekeeping stuff.  */
-#define TIMERTICK_INTERVAL         (60)
-
-/* How oft to run the housekeeping.  */
-#define HOUSEKEEPING_INTERVAL      (600)
-
-
 /* This union is used to avoid compiler warnings in case a pointer is
    64 bit and an int 32 bit.  We store an integer in a pointer and get
    it back later (npth_getspecific et al.).  */
@@ -1766,83 +1759,6 @@ handle_signal (int signo)
 #endif /*!HAVE_W32_SYSTEM*/
 
 
-/* Thread to do the housekeeping.  */
-static void *
-housekeeping_thread (void *arg)
-{
-  static int sentinel;
-
-  (void)arg;
-
-  if (sentinel)
-    {
-      log_info ("housekeeping is already going on\n");
-      return NULL;
-    }
-  sentinel++;
-  if (opt.verbose > 1)
-    log_info ("starting housekeeping\n");
-
-  if (opt.verbose > 1)
-    log_info ("ready with housekeeping\n");
-  sentinel--;
-  return NULL;
-
-}
-
-
-#if GPGRT_GCC_HAVE_PUSH_PRAGMA
-# pragma GCC push_options
-# pragma GCC optimize ("no-strict-overflow")
-#endif
-static int
-time_for_housekeeping_p (time_t curtime)
-{
-  static time_t last_housekeeping;
-
-  if (!last_housekeeping)
-    last_housekeeping = curtime;
-
-  if (last_housekeeping + HOUSEKEEPING_INTERVAL <= curtime
-      || last_housekeeping > curtime /*(be prepared for y2038)*/)
-    {
-      last_housekeeping = curtime;
-      return 1;
-    }
-  return 0;
-}
-#if GPGRT_GCC_HAVE_PUSH_PRAGMA
-# pragma GCC pop_options
-#endif
-
-
-/* This is the worker for the ticker.  It is called every few seconds
-   and may only do fast operations. */
-static void
-handle_tick (void)
-{
-  if (time_for_housekeeping_p (gnupg_get_time ()))
-    {
-      npth_t thread;
-      npth_attr_t tattr;
-      int err;
-
-      err = npth_attr_init (&tattr);
-      if (err)
-        log_error ("error preparing housekeeping thread: %s\n", strerror (err));
-      else
-        {
-          npth_attr_setdetachstate (&tattr, NPTH_CREATE_DETACHED);
-          err = npth_create (&thread, &tattr, housekeeping_thread, NULL);
-          if (err)
-            log_error ("error spawning housekeeping thread: %s\n",
-                       strerror (err));
-          npth_attr_destroy (&tattr);
-        }
-    }
-}
-
-
 /* Check the nonce on a new connection.  This is a NOP unless we are
    using our Unix domain socket emulation under Windows.  */
 static int
@@ -1943,9 +1859,6 @@ handle_connections (assuan_fd_t listen_fd)
   gnupg_fd_t fd;
   int nfd, ret;
   fd_set fdset, read_fdset;
-  struct timespec abstime;
-  struct timespec curtime;
-  struct timespec timeout;
   int saved_errno;
   int my_inotify_fd = -1;
 
@@ -1985,9 +1898,7 @@ handle_connections (assuan_fd_t listen_fd)
 #endif /*HAVE_INOTIFY_INIT*/
 
 
-  /* Setup the fdset.  It has only one member.  This is because we use
-     pth_select instead of pth_accept to properly sync timeouts with
-     to full second.  */
+  /* Setup the fdset.  */
   FD_ZERO (&fdset);
   FD_SET (FD2INT (listen_fd), &fdset);
   nfd = FD2INT (listen_fd);
@@ -1998,9 +1909,6 @@ handle_connections (assuan_fd_t listen_fd)
         nfd = my_inotify_fd;
     }
 
-  npth_clock_gettime (&abstime);
-  abstime.tv_sec += TIMERTICK_INTERVAL;
-
   /* Main loop.  */
   for (;;)
     {
@@ -2011,7 +1919,7 @@ handle_connections (assuan_fd_t listen_fd)
             break; /* ready */
 
           /* Do not accept new connections but keep on running the
-           * loop to cope with the timer events.
+           * select loop to wait for signals (e.g. SIGCHLD).
            *
            * Note that we do not close the listening socket because a
            * client trying to connect to that socket would instead
@@ -2031,24 +1939,14 @@ handle_connections (assuan_fd_t listen_fd)
       /* Take a copy of the fdset.  */
       read_fdset = fdset;
 
-      npth_clock_gettime (&curtime);
-      if (!(npth_timercmp (&curtime, &abstime, <)))
-	{
-	  /* Timeout.  */
-	  handle_tick ();
-	  npth_clock_gettime (&abstime);
-	  abstime.tv_sec += TIMERTICK_INTERVAL;
-	}
-      npth_timersub (&abstime, &curtime, &timeout);
-
 #ifndef HAVE_W32_SYSTEM
-      ret = npth_pselect (nfd+1, &read_fdset, NULL, NULL, &timeout, npth_sigev_sigmask());
+      ret = npth_pselect (nfd+1, &read_fdset, NULL, NULL, NULL, npth_sigev_sigmask());
       saved_errno = errno;
 
       while (npth_sigev_get_pending(&signo))
 	handle_signal (signo);
 #else
-      ret = npth_eselect (nfd+1, &read_fdset, NULL, NULL, &timeout, NULL, NULL);
+      ret = npth_eselect (nfd+1, &read_fdset, NULL, NULL, NULL, NULL, NULL);
       saved_errno = errno;
 #endif
 
@@ -2062,8 +1960,7 @@ handle_connections (assuan_fd_t listen_fd)
 
       if (ret <= 0)
         {
-          /* Interrupt or timeout.  Will be handled when calculating the
-             next timeout.  */
+          /* Interrupt.  Will be handled at the top of the next loop.  */
           continue;
         }
 
